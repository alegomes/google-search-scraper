const GoogleSearch = require('./main');
const CREDENTIALS = require('./input/credentials');
const winston = require('winston')

GoogleSearch._setup('validate_account')

const myFormat = winston.format.printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${level}] ${message}`;
});

const tzoffset = (new Date()).getTimezoneOffset() * 60000;
const timestamp = (new Date(Date.now() - tzoffset)).toISOString().substring(0,19 ).replace(/[T\-\:]/g,'');

// const logfile_all = `output/${timestamp}/validate_accounts.${timestamp}.all.log`
// const logfile_info = `output/{timestamp}/validate_accounts.${timestamp}.info.log`

// const GoogleSearch.logger = winston.createGoogleSearch.logger({
//     level: 'info',
//     format: winston.format.combine(
//     winston.format.timestamp({
//         format: 'YYYY-MM-DD HH:mm:ss'
//     }),
//     winston.format.errors({ stack: true }),
//     winston.format.splat(),
//     winston.format.json(),
//     // winston.format.colorize(),
//     myFormat
//     ),
//     defaultMeta: { service: 'google-search-scraper-accounts-validation' },
//     transports: [
//     // https://github.com/winstonjs/winston#logging-levels
//     new winston.transports.File({ filename: logfile_all, level: 'silly' }),
//     new winston.transports.File({ filename: logfile_info, level: 'info' }),
//     new winston.transports.Console()
//     ]
// });

// GoogleSearch.GoogleSearch.logger = GoogleSearch.logger

letsvalidate = async () => {
    for (let k in CREDENTIALS) {
        // log(`${k} -> ${JSON.stringify(CREDENTIALS[k])}\n`)
        
        let email = CREDENTIALS[k].email
        let passwd = CREDENTIALS[k].passwd
        let phone = CREDENTIALS[k].phone
            
        GoogleSearch.logger.debug(`Is ${email} verified? `, { label : 'letsvalidate'})
        if (! await GoogleSearch.is_verified(email, passwd, phone)) {
            GoogleSearch.logger.debug(`Is ${email} verified? NO`, { label : 'letsvalidate'})

            try {
                GoogleSearch.logger.debug(`Verifying account ${email}`, { label : 'letsvalidate'})
                await GoogleSearch.verify_account(phone)
                
                let timestamp_fmt = (new Date(Date.now() - tzoffset)).toISOString().substring(0,19)
                var smsmsg = `SMS sent at ${timestamp_fmt}`
                GoogleSearch.logger.debug(smsmsg, { label : 'letsvalidate'})
                console.log(smsmsg)

            } catch (e1) {
                let msg = `Fail to verify account ${email}`
                await GoogleSearch.close()
                GoogleSearch.logger.error(msg, { label : 'letsvalidate'})
                GoogleSearch.logger.error('---> '+e1, { label : 'letsvalidate'})
                console.log('Log files available at ' + GoogleSearch._get_root_path())
                throw msg
            }
            
            let sms = read_received_sms()
            // .then(async (d) => {

                if (! sms) {
                    throw 'SMS code required'
                }

                GoogleSearch.logger.info(`Sending code G-${sms}`, { label : 'letsvalidate'})

                try {
                    const elementHandle = await GoogleSearch.page.$('#idvPreregisteredPhonePin');
                    await elementHandle.type(sms, {delay: 100});
                    GoogleSearch.logger.silly('SMS G-Code filled successfuly.')
                    await GoogleSearch._screenshot('gcode_filled');
                    GoogleSearch.logger.silly('Sending g-code number...')
                    await elementHandle.press('Enter');
                    GoogleSearch.logger.silly('G-code sent successfuly.')
                    await GoogleSearch._screenshot('gcode_sent');
                    const tzoffset = (new Date()).getTimezoneOffset() * 60000;

                    if (await GoogleSearch._is_user_authenticated()) { 
                        GoogleSearch.logger.info(`User ${email} verified and authenticated. Congratz.`, { label : 'letsvalidate'})
                        return 
                    } 
                    
                    GoogleSearch.logger.silly(`User ${email} not authenticated yet.`, { label : 'letsvalidate'})

                    try {
                        GoogleSearch.logger.silly(`"Protect your account" page, maybe?`, { label : 'letsvalidate'})
                        await GoogleSearch.page.waitFor("//div[@class='N4lOwd' and contains(text(),'Proteger sua conta') ]")
                        GoogleSearch.logger.silly(`Yes. Waiting on "Protect your account" page.`, { label : 'letsvalidate'})
                        GoogleSearch.logger.silly(`Clicking on Concluir...`, { label : 'letsvalidate'})
                        // await GoogleSearch.page.click("//div[@role='button']//span[contains(text(),'Concl')]") // Nao funciona com xpath
                        await GoogleSearch.page.click('div.yKBrKe > div[role=button]')
                        
                    } catch (e) {
                        GoogleSearch.logger.error(`Fail to interact with "Protect your account" page: ${e}`, { label : 'letsvalidate'})
                        throw e
                    }
                    
                    try {
                        if (await GoogleSearch._is_user_authenticated()) {  // TODO Trocar o retorno booleano por exception
                            GoogleSearch.logger.info(`User ${email} verified and authenticated. Congratz.`, { label : 'letsvalidate'})
                            return 
                        } else {
                            throw `User ${email} not authenticated.`
                        }
                    } catch (e) {
                        throw 'Fail to verify if user is authenticated: ' + e
                        
                    }
                    


                } catch(e1) {
                    GoogleSearch.logger.error(`Fail sending g-code ${sms}: ${e1}`, { label : 'letsvalidate'})
                    throw e1
                }

                GoogleSearch.logger.info('FIM:' + sms)
                return
            // }).then((d) => {
            //     GoogleSearch.logger.info('FIM:' + d)
            // })

        } else {
            GoogleSearch.logger.debug(`Is ${email} verified? YES`, { label : 'letsvalidate'})
            GoogleSearch.logger.error('No account verification required this time.', { label : 'letsvalidate'})
        } // if
    } // for
}

read_received_sms = function() {

    GoogleSearch.logger.info("Please type received SMS validation code: ");

    var readlineSync = require('readline-sync');
    return readlineSync.question('G-XXXXXX: ');

    // var readline = require('readline');
    // var resp = "";

    // var leitor = readline.createInterface({
    //     input: process.stdin,
    //     output: process.stdout
    // });

    // return new Promise(
    //     resolve => leitor.question("G-XXXXXX: ", 
    //         ans => {
    //             leitor.close();
    //             resolve(ans);
    //         })
    //     )

}

letsvalidate().then( d => { 
    console.log('-------------------------------')    
    console.log('FIM') 
 }).catch( r => { 
        console.log('ERROR: ' + r)
}).finally( () => {
    console.log('Verifique o resultado em:')
    console.log(`  ${GoogleSearch._get_root_path()}/\n\n`)
    GoogleSearch.close()
})


