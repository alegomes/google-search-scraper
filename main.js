const puppeteer = require('puppeteer');
const { URL } = require('url');
const fse = require('fs-extra'); // v 5.0.0
const path = require('path');
const fs = require('fs')
const winston = require('winston');
const matchAll = require('string.prototype.matchall');


// debugger;

GoogleSearch = {

    PASSWD_SELECTOR : '#Passwd',
    PHONEKNOWLEDGE_SELECTOR : 'img[src="//ssl.gstatic.com/accounts/marc/phoneknowledge.png"]',
    IDVCALL_SELECTOR : 'img[src="//ssl.gstatic.com/accounts/marc/idv_call.png"]',
    IDVSMS_SELECTOR : 'img[src="//ssl.gstatic.com/accounts/marc/idv_sms.png"]',
    browser : null,
    page : null,
    logger : null,
    
    _login : async function(email, passwd, phone) {
      this.logger.info('Logging in ' + email, { label : '_login'})
      await GoogleSearch._open_login_page();
      if (! await GoogleSearch._is_user_authenticated(email)) {
        await GoogleSearch._fill_in_email(email);
        await GoogleSearch._fill_in_password(passwd, phone); 

        if(! await this._is_user_authenticated(email)) {
          await this._check_if_account_verification_page()
        }
      }
      
      this.logger.info(email + ' logged in.', { label : '_login'})
    },

    _open_login_page : async function() {

      this.logger.verbose('Opening Google sign in page...', { label : '_open_login_page'} );

      // const LOGIN_URL = 'https://accounts.google.com/signin/v2/identifier?hl=en&passive=true&continue=https%3A%2F%2Fwww.google.com%2F&flowName=GlifWebSignIn&flowEntry=ServiceLogin';
      const LOGIN_URL = 'https://accounts.google.com/signin'

      this.page = await this._get_page();
  
      try {    
          await this.page.goto(LOGIN_URL, {waitUntil: ['load', 'domcontentloaded', 'networkidle2']}); //TODO Testar { waitUntil: 'networkidle0' } load, que mais?

          await this._screenshot('login_page')
          await this._savepage('login_page')  
        } catch (e) {
          this.logger.error('Error opening login page: ' + e, { label : '_open_login_page'});
          throw e;
        }
         
    },

    _fill_in_email:  async function(email) {

      this.logger.debug(`Filling email ${email}...`, { label : '_fill_in_email'})

      this._email = email

      // const EMAIL_SELECTOR = '#Email';
      const EMAIL_SELECTOR = ['#Email','#identifierId'];
      // const EMAIL_NEXT_SELECTOR = '#next';
      const EMAIL_NEXT_SELECTOR = ['#next','#identifierNext'];

      try {
        this.logger.silly('Waiting for ' + EMAIL_NEXT_SELECTOR[0], { label : '_fill_in_email'})
        await this.page.waitFor(EMAIL_NEXT_SELECTOR[0])
        this.logger.silly(`Found ${EMAIL_NEXT_SELECTOR[0]}`, { label : '_fill_in_email'})
        this._hasJS=0

      } catch (e) {
        this.logger.silly(`Did not found ${EMAIL_NEXT_SELECTOR[0]}: ${e}`, { label : '_fill_in_email'})

            try {
              this.logger.silly('Waiting for ' + EMAIL_NEXT_SELECTOR[1], { label : '_fill_in_email'})
              await this.page.waitFor(EMAIL_NEXT_SELECTOR[1])

              this.logger.silly(`Found ${EMAIL_NEXT_SELECTOR[1]}`, { label : '_fill_in_email'})
              this._hasJS=1

            } catch (e2) {
              this.logger.warn(`Could not find ${EMAIL_NEXT_SELECTOR[1]}: [${e}]`, { label : '_fill_in_email'})
            }
      }
      await this._screenshot('email_page');
      await this._savepage('email_page');
      
      // console.log('Selecting email field...');
      // await this.page.click(EMAIL_SELECTOR);

      this.logger.debug('Typing email ' + email, { label : '_fill_in_email'});
      
      // ERRO: Digita so um caracter
      // await this.page.keyboard.type(email);

      // ERRO: Nao encontra o campo
      // await page.evaluate((text) => { (document.getElementById('#identifierId')).value = text; }, email);
      
      // ERRO: Digita so um caracter
      // const input = await this.page.$(EMAIL_SELECTOR);
      // await input.press('Backspace');
      // await input.type(email);

      await this.page.type(EMAIL_SELECTOR[this._hasJS], email); //TODO Qual selector sera que ele te pegando?          
      await this._screenshot('email_typed');
      
      try {
        this.logger.debug(`Clicking ${EMAIL_NEXT_SELECTOR[this._hasJS]}...`, { label : '_fill_in_email'});
        await this.page.click(EMAIL_NEXT_SELECTOR[this._hasJS]); 
        this.logger.debug(`Clicked on ${EMAIL_NEXT_SELECTOR[this._hasJS]}`, { label : '_fill_in_email'})
      } catch (e) {
        this.logger.error(`Failed to click ${EMAIL_NEXT_SELECTOR[this._hasJS]}`, { label : '_fill_in_email'})
        throw e
      }
      
      try {
        this.logger.debug(`Waiting next page with ${this.PASSWD_SELECTOR}...`, { label : '_fill_in_email'})
        await this.page.waitForSelector(this.PASSWD_SELECTOR)
        this.logger.debug(`Found ${this.PASSWD_SELECTOR}`, { label : '_fill_in_email'})
        await this._screenshot('email_submitted')
      } catch (e) {
        this.logger.error('Failed to send email: ' + e, { label : '_fill_in_email'})
        await this._screenshot('email_submitted_navigation_failed')
        await this._savepage('email_submitted_navigation_failed')

        try {
          this.logger.debug(`Unknown email, maybe?`, { label : '_fill_in_email'})
          await this.page.waitFor('//*[contains(text(),"O Google não reconhece o e-mail")]')
          this.logger.debug(`Email not recognized.`, { label : '_fill_in_email'})
          await this._screenshot('email_not_recognized')
        } catch (e1) {
          this.logger.debug(`Couldn't check if email was not recognized: ${e1}`, { label : '_fill_in_email'})
          this.logger.debug('This is why email submission has failed: ' + e, { label : '_fill_in_email'})
          throw e
        }

        throw `Email ${email} not recognized`
      }
        
    },

    _fill_in_password : async function(passwd, phone) {

      this.logger.debug('Filling password...', { label : '_fill_in_password'})

      const PASSWD_SELECTOR = '#Passwd'; //#password > div.aCsJod.oJeWuf > div > div.Xb9hP > input
      const PASSWD_NEXT_SELECTOR = '#signIn'; // TODO Estar preparado para pagina com e sem JS

      await this._screenshot('password_page');
      this.logger.debug('Typing password...', { label : '_fill_in_password'});
      await this.page.click(PASSWD_SELECTOR);
      await this.page.keyboard.type(passwd);
      await this._screenshot('password_typed');
      this.logger.debug('Clicking Sign in button...', { label : '_fill_in_password'});
      await this.page.click(PASSWD_NEXT_SELECTOR);
      this.logger.debug('Password submitted. Waiting for navigation...', { label : '_fill_in_password'})
      
    },

    _check_if_account_verification_page: async function() {
      this.logger.verbose(`Checking if account ${this._email} needs to be verified...`, { label : '_check_if_account_verification_page'})
      
      try {
        // Tela pedindo pra verificar a conta através de ligação, SMS ou confirmando o número cadastrado.
        await this.page.waitForSelector('#challengePickerList')
      } catch (e) {
        this.logger.verbose(`Account ${this._email} DOESN'T need to be verified: ${e}`, { label : '_check_if_account_verification_page'})
        
        this.logger.debug('Is there a captcha to be solved?', { label : '_check_if_account_verification_page'}) // TODO
        this.logger.debug('  ...don\'t know to to handle it yet.', { label : '_check_if_account_verification_page'})
        
        this._screenshot('verification_not_needed')
        this._savepage('verification_not_needed')

        await this._check_if_suspicious();
      }

      this._screenshot('verification_needed')
      this._savepage('verification_needed')

      let msg = `Account ${this._email} needs to be verified.`
      this.logger.error(msg, { label : '_check_if_account_verification_page'}, { label : '_check_if_account_verification_page'})
      throw msg
    },

    _verification_needed: async function() {

      try {
        await this._check_if_account_verification_page()
        return false
      } catch (e) {
        return true
      }

      // try {
      //   // Verifica se eh a tela insistindo na confirmacao por SMS ou ligacao
      //   this.logger.debug('Checking if verification needed...')
      //   await this.page.waitFor('//*[contains(text(),"Confirme")]')
      //   this.logger.silly('Yes. A new verification step is needed.')

      // } catch (e3) {
      //   this.logger.silly('No. A new verification step is NOT expected. So, what?: ' + e3)

      //   this._screenshot('verification_not_needed')
      //   this._savepage('verification_not_needed')

      //   return false
      // }
          
      // this._screenshot('verification_needed')
      // this._savepage('verification_needed')

      // return true
    },

    is_verified: async function(email, passwd, phone) {
      this.logger.info(`\n\nTesting if ${email} if verified...............`, {label: 'is_verified'})
        
      try {
          this.logger.debug(`Openning login page`, {label: 'is_verified'})
          await this._open_login_page();
          this.logger.debug(`Is user authenticated?`, {label: 'is_verified'})
          if (await this._is_user_authenticated(email)) {
            this.logger.debug(`Is user authenticated? YES`, {label: 'is_verified'})
            return true
          }

          this.logger.debug(`Is user authenticated? NO`, {label: 'is_verified'})
          
          this.logger.debug(`Filling email`, {label: 'is_verified'})
          await this._fill_in_email(email);
          this.logger.debug(`Filling password`, {label: 'is_verified'})
          await this._fill_in_password(passwd, phone);  

          this.logger.debug(`Is user authenticated (again)?`, {label: 'is_verified'})
          if(await this._is_user_authenticated(email)) {
            this.logger.debug(`Is user authenticated (again)? YES`, {label: 'is_verified'})
            this.logger.info(email + ' is verified and loggeed in.', { label : '_login'})
            return true
          }

          this.logger.debug(`Is user authenticated (again)? NO`, {label: 'is_verified'})

      } catch (e) {
        let msg = `Error checking if account ${email} needs to be verified: ${e}`
        this.logger.error(msg, {label : 'is_verified'})
        throw msg
      }

      try {
        this.logger.debug(`Verification page?`, {label: 'is_verified'})
        await this._check_if_account_verification_page()
        this.logger.debug(`Verification page? NO`, {label: 'is_verified'})
      } catch (e) {
          this.logger.debug(`Verification page? YES`, {label: 'is_verified'})
          this.logger.info(`Testing if ${email} is verified............... NOK`, {label: 'is_verified'})
          this.logger.debug(`\n\n${e}\n`, {label: 'is_verified'})
          if (e.stack) {
              GoogleSearch.logger.debug(e.stack, {label: 'is_verified'})        
          }
          // var e = 'Account resocie.neutro@gmail.com waiting for SMS validation.'
          // var e = 'Account temporarily blocked.'

          // let msg = `Account ${email} waiting for SMS.*`
          let msg = `Account ${email} needs to be verified`
          let regex = new RegExp(msg)
          let match = e.match(regex);

          if(match) {
            this.logger.debug(`Account ${email} NOT verified.`, {label: 'is_verified'})
            return false
          } else {
            let msg = `Account ${email} could not be authenticated and doesn't seem to need verification: ${e}`
            this.logger.error(msg)
            throw msg
          }
        }

    },

    _check_if_account_blocked : async function() {

      let ctx = { label: '_check_if_account_blocked'}

      this.logger.verbose('Account blocked?', ctx)

      try {
        // Sera que travou o acesso a conta?
        await this.page.waitFor('//p[contains(text(),"Número de tentativas de login excedido.")]')
        this.logger.warn('Yes. Account blocked. Gotta wait.', ctx)
      } catch (e) {
        this.logger.debug('Account doesn\'t seem to be temporarily blocked: ' + e, ctx)
        return
      }

      this._screenshot('account_temporarily_blocked')
      this._savepage('account_temporarily_blocked')
      
      throw 'Account temporarily blocked.'
    },

    _sms_verification_available: async function() {
      this.logger.verbose('Account verification by SMS available?')

      try {
        await this.page.waitForSelector(this.IDVSMS_SELECTOR)
        this.logger.debug('SMS verification available')
        return true
      } catch (e) {
        this.logger.error('SMS verification NOT available: ' + e)
        return false
      }
    },

    _phoneknowledge_verification_available: async function() {
      this.logger.verbose('Account verification by phone knowledge available?')

      try {
        await this.page.waitForSelector(this.PHONEKNOWLEDGE_SELECTOR)
        this.logger.debug('Phone knowledge verification available')
        return true
      } catch (e) {
        this.logger.error('Phone knowledgeSMS verification NOT available: ' + e)
        return false
      }
    },

    _phonecall_verification_available: async function() {
      this.logger.verbose('Account verification by phone call available?')

      try {
        await this.page.waitForSelector(this.IDVCALL_SELECTOR)
        this.logger.debug('Phone call verification available')
        return true
      } catch (e) {
        this.logger.error('Phone call verification NOT available: ' + e)
        return false
      }
    },

    _verify_by_sms: async function(phone) {
      this.logger.verbose(`Verifying account ${this._email} by SMS`)

      let selectors = ['#idvPreregisteredPhonePin'] // pt.BR
      let texts = ['Get a verification code at'] // en.EN

      try {
        this.logger.silly(`Requesting SMS to be sent to ${phone}`)
        // pt.BR Receber uma mensagem de texto com um código de verificação no número ...
        // en.EN Get a verification code at ...
        //       li.JDAKTe:nth-child(1) > div:nth-child(1)
        // await this.page.click('#challengePickerList > li:nth-child(1) > form > button')
        this.logger.debug('Choosing SMS option...')
        
        // await this.page.click(this.IDVSMS_SELECTOR)

        var tzoffset = (new Date()).getTimezoneOffset() * 60000;
        // let timestamp_fmt = (new Date(Date.now() - tzoffset)).toISOString().substring(0,19)
        // var smsmsg = `SMS sent at ${timestamp_fmt}`

        const handlers = await this.page.$x("//*[contains(text(), 'Receber uma mensagem de texto com um código de verificação no número ')]");
        console.log('handlers='+handlers)
        if (handlers && handlers.length > 0) {
          console.log('1. '+ (new Date(Date.now() - tzoffset)).toISOString().substring(0,19))
          handlers[0].click();
        } else {
          throw new Error("SMS option not found");
        }
        
        // await this.page.waitForSelector('input[name="phoneNumber"]')                    
        await this.page.waitForSelector('#idvPreregisteredPhonePin') 

        this.logger.debug(`SMS code sent to ${phone} to verify ${this._email}.`)

        this._screenshot('sms_requested')
        this._savepage('sms_requested')

      } catch (e4) {
        this.logger.error('Failed to request SMS for account verification: ' + e4)

        try {
          this.logger.debug('Trying with another selector...')
          await this.page.waitForSelector('#idvPin') 

          this.logger.debug(`SMS code sent to ${phone} to verify ${this._email}.`)

          this._screenshot('sms_requested')
          this._savepage('sms_requested')
        } catch (e5) {
          this.logger.error('Failed a 2nd time to request SMS for account verification: ' + e5)
          console.log('2. '+(new Date(Date.now() - tzoffset)).toISOString().substring(0,19))
          throw e5
        }

        
      }

      this.logger.info(`Account ${this._email} waiting for SMS code sent to ${phone}`) // TODO Fluxo de sucesso com exception?!?!

    },

    _verify_by_phoneknowledge: async function(phone) {
      this.logger.verbose(`Verifying account ${this._email} by phone knowledge`)

      try {
        await this.page.click(this.PHONEKNOWLEDGE_SELECTOR)
        await this.page.waitForSelector('input[name="phoneNumber"]')
        this.logger.silly('Phone knowledge option chosen successfuly.')
      } catch (e1) {
        this.logger.error('Account verification failed when selecting phone knowledge option: ' + e1)
        throw e1
      }
      
      await this._screenshot('phone_knowledge')
      await this._savepage('phone_knowledge')

      try {
        this.logger.silly('Selecting phone number field...')
        await this.page.click('input[name="phoneNumber"]')

      //   // Informa o numero de telefone cadastrado
        const elementHandle = await this.page.$('input[name="phoneNumber"]');
        await elementHandle.type(phone, {delay: 100});
        this.logger.silly('Phone number filled successfuly.')
        await this._screenshot('phone_filled');
        this.logger.silly('Sending phone number...')
        await elementHandle.press('Enter');
        this.logger.silly('Phone number sent successfuly.')
      } catch (e2) {
        this.logger.warn('Fail sending phone number for phone knowledge account verification: ' + e2)
        throw e2
      }

      try {
        await this._screenshot('phone_sent')
        await this._savepage('phone_sent')
      } catch (e) {
        this.logger.warn('Fail to save "phone_sent" page screenshot/html. I don\'t care :-): ')
        this.logger.debug(e)
      }

      this.logger.info(`Phone knoledge verification done.`) // TODO Fluxo de sucesso com exception?!?!

    },

    _verify_by_phonecall: async function(phone) {
      this.logger.verbose('Account verification by phone call available?')

      // TODO
    },

    // https://support.google.com/accounts/answer/2506340?p=sign_in_prevented_csa2&hl=pt-BR&visit_id=637061554323777064-3501235620&rd=1
    verify_account : async function(phone) {

      let ctx = {label : 'verify_account'}

      this.logger.verbose('Verifying account', ctx)

      this._screenshot('verify_options')
      this._savepage('verify_options')

      this.logger.debug('Checking if account blocked', ctx)
      await this._check_if_account_blocked()
      this.logger.debug('Account not blocked', ctx)

      let verification_needed = await this._verification_needed()
      this.logger.debug(`Verification needed?: ${verification_needed}`, ctx)
      while (verification_needed) {

        this.logger.debug(`Account verification needed for ${this._email}`, ctx)

        try {

        
        this.logger.debug('SMS verification available?', ctx)
        if (await this._sms_verification_available()) {
          
          this.logger.debug('Starting SMS verification', ctx)
          await this._verify_by_sms(phone)
          verification_needed = false
          this.logger.debug('SMS verification ended', ctx)

        } else {

          this.logger.debug('Phone knowledge verification available?', ctx)
          if (await this._phoneknowledge_verification_available(phone)) {

            this.logger.debug('Starting phone knowledge verification', ctx)
            await this._verify_by_phoneknowledge(phone)
            verification_needed = false
            this.logger.debug('Phone knowledge verification ended', ctx)

          } else {

            this.logger.debug('Phone call verification available?', ctx)
            if (await this._phonecall_verification_available(phone)) {

              this.logger.debug('Starting phone call verification', ctx)
              await this._verify_by_phonecall(phone)
              verification_needed = true
              this.logger.debug('Phone call verification ended', ctx)

            } 
          }
        }
      } catch (e) {
        verification_needed = await this._verification_needed()
        this.logger.debug(`Verification needed?: ${verification_needed}`, ctx)
      }

        
      }
      this.logger.debug(`Account verification nod needed for ${this._email}`, ctx)


      
      // try {
      //   // Aguarda pela tela 'Ola, Fulano de Tal'
      //   this.logger.silly('Checking if account was verified and user was authenticated.')
      //   // await this.page.waitForSelector('h1#x7WrMb')
      //   await this.page.waitFor('//h1[contains(text(),"Bem-vindo")]')
      //   this.logger.debug(`Account ${this._email} verified and authenticated.`)
      //   this.screenshot('account_verified')
      //   return 
      // } catch (e) {
      //   this.logger.warn(`Account ${this._email} not verified yet`)
      //   this.logger.debug(e)
      // }

      // await this._screenshot('not_verified')
      // await this._savepage('not_verified')

      


      // 16/10/2019 Temporariamente comentado ate eu me lembrar pra que serve
      // try {
      //   await this.page.waitFor('//*[contains(text(),"Escolha uma conta")]')
      //   this.logger.warn('Select account screen')
      // } catch (e5) {
      //   this.logger.error(e5)
      //   throw e5
      // }
    },

    // TODO duplicado no validate_accounts.js?
    validate_account : async function() {
      try {
        this.logger.silly('Filling G-CODE...')
        const elementHandle = await this.page.$('#idvPreregisteredPhonePin');
        await elementHandle.type(gcode, {delay: 100});
        await this._screenshot('gcode_filled2');
        this.logger.silly(`Sending G-CODE ${gcode}`)
        await elementHandle.press('Enter');
        this.logger.silly('G-CODE sent successfuly.')
        await this.page.waitForSelector('#idvPreregisteredPhonePin') //Campo pra informar o codigo enviado por SMS
      } catch (e6) {
        this.logger.error('Error sending G-CODE: ' + e6)
        throw e6
      }
          
      try {
        // Aguarda pela tela 'Ola, Fulano de Tal'
        // await this.page.waitForSelector('h1#x7WrMb')
        await this.page.waitFor('//h1[contains(text(),"Bem-vindo")]')
        this.logger.verbose(`Account ${this._email} finally verified and authenticated.`)
        this.screenshot('account_verified2')
        return
      } catch (e7) {
        this.logger.error(`Dhammm. Account ${this._email} not validated yet: ${e7}`)
        throw e7
      }
    },

    _open_google_search : async function() {

      this.logger.verbose('Opening Google Search page')

      const GOOGLE_SEARCH_URL = 'https://www.google.com';

      await this.page.goto(GOOGLE_SEARCH_URL, {waitUntil: 'load'});
      // await this.page.waitForNavigation(4444);
      // await this.page.screenshot({path: './screenshots/google_search_home.png'});
      await this._screenshot('home');
        
    },

    _fill_in_search_string : async function(q) {

      this.logger.verbose(`Filling search string "${q}"`)

      try {
        // const QUERY_FIELD_SELECTOR = '#tsf > div:nth-child(2) > div > div.RNNXgb > div > div.a4bIc > input';
        const QUERY_FIELD_SELECTOR = 'input[name="q"]'
        // const SEARCH_BUTTON_SELECTOR = '#tsf > div:nth-child(2) > div > div.FPdoLc.VlcLAe > center > input.gNO89b';
        const SEARCH_BUTTON_SELECTOR = 'input[name="btnG"]'

        await this._screenshot('before_searching');
        await this.page.click(QUERY_FIELD_SELECTOR);
        
        const elementHandle = await this.page.$(QUERY_FIELD_SELECTOR);
        await elementHandle.type(q, {delay: 100});
        await this._screenshot('search_string');
        await elementHandle.press('Enter');

        // await this.page.keyboard.type(q);
        // await this.page.screenshot({path : './screenshots/google_search_string.png'}) // TODO Capturar sugestoes
        // await this.page.click(SEARCH_BUTTON_SELECTOR);

        await this.page.waitForNavigation();

      } catch (e) {
        this.logger.error(`Error when filling in search string "${q}": ${e}` )
        throw e
      }
    },

    _get_results : async function(query) {

      this.logger.verbose(`Getting results from "${query}" search`)

      // $$("div.ZINbbc.xpd.O9g5cc.uUPGi > div.kCrYT + div.x54gtf + div.kCrYT")[0].parentNode

      // Gets result descriptions
      // const DESCRIPTIONS_FIELD_SELECTOR = 'div.ZINbbc.xpd.O9g5cc.uUPGi > div.kCrYT + div.x54gtf + div.kCrYT' 
      // const DESCRIPTIONS_FIELD_SELECTOR = 'div.ZINbbc.xpd.O9g5cc.uUPGi' 
      const RESULTS_SELECTOR = 'div.rc'

      // await this.page.screenshot({path: './screenshots/google_search_results_getresults.png'});
      let suffix = 'search_results_'+query.replace(' ', '_')
      await this._screenshot(suffix);
      await this._saveaspdf(suffix);
      await this._savepage(suffix);
      
      // const url = new URL(response.url());
      // const urls = new URL(this.page.url());
      // let filePath = path.resolve(`./output${urls.pathname}`);
      // if (path.extname(urls.pathname).trim() === '') {
      //   filePath = `${filePath}/index.html`;
      // }
      // //await fse.outputFile(filePath, await response.buffer());
      // await fse.outputFile(filePath, await this.page.content());

      const elements = await this.page.$$(RESULTS_SELECTOR)

      var results = []
      for(var i in elements) {
        //$$('div.rc')[0].querySelectorAll('div.r a').getProperty('href')
        var url_wrapper = await elements[i].$('div.r a')
        var url = await url_wrapper.getProperty('href')
        GoogleSearch.logger.silly(`[_get_results] url1=${url}`)
        var url = await this.page.evaluate(e => e.getAttribute('href'), url_wrapper)
        GoogleSearch.logger.silly(`[_get_results] url2=${url}`)

        // var url1 = await url.getProperty('href')
        // var url2 = await elements[i].$('div.r a').then((e) => { return e.getProperty('href') })
        
        //$$('div.rc')[0].querySelector('div.r div.ellip').innerText
        
        var title_wrapper = await elements[i].$('div.r div.ellip')
        if(!title_wrapper) {
          title_wrapper = await elements[i].$('div.r h3')
        }
        
        var title = await this.page.evaluate(e => e.textContent, title_wrapper);
        GoogleSearch.logger.silly(`[_get_results] title=${title}`)

        //$$('div.rc')[0].querySelector('div.s').innerText
        var description_wrapper = await elements[i].$('div.s')
        var description = await this.page.evaluate(e => e.textContent, description_wrapper)
        GoogleSearch.logger.silly(`[_get_results] description=${description}`)

        // TODO A ordem ta vindo diferente. Deve ser pq os resultados estao dentro de 
        // divs diferentes div.bkWMgd
        var result = {
          'timestamp' : this.timestamp_fmt,
          'user' : this._email.slice(0,this._email.indexOf('@')),
          'query' : query,
          'position' : (parseInt(i)+1),
          'title' : title,
          'description' : description,
          'url' : url,
          // 'created_at': ,
          // 'updated_at': 
        }

        // console.log(result)

        this.logger.debug(`result=${JSON.stringify(result)}`)
        results.push(result) 
      }

      this.logger.verbose(`Returning ${results.length} results`)
      return results
    },

    _get_sponsored: async function() {
      const SPONSORED_SELECTOR = 'div.cu-container'

      const results = await this.page.$$(SPONSORED_SELECTOR)
      return results
    },

    _get_images: async function() {
      //const IMAGES_SELECTOR = 'div.GNxIwf div[jscontroller="xc1DSd"]' //.img-brk parece mais semanticamente correto
      const IMAGES_SELECTOR = 'g-inner-card'

      const results = await this.page.$$(IMAGES_SELECTOR)
      return results
    },

    close : async function() {

        this.logger.info('Closing stuff...')

        if (this.page && !this.page.isClosed()) {
          try {
            await this._screenshot('before_closing_page')
          } catch (e) {
            this.logger.warn('Error screenshotting before_closing_page. Never mind...: ' + e)
          } 

          try {
            await this._savepage('before_closing_page')
          } catch (e) {
            this.logger.warn('Error saving html before_closing_page. Never mind...: ' + e)
          } 

          this.logger.silly('Closing page...');
          await this.page.close();
          this.logger.silly('Closing browser...');
          await this.browser.close();
          this.logger.silly('All closed.');
        }

        //delete(this._timestamp)
        // delete(this._screenshots_counter)
        // delete(this._htmls_counter)
        // delete(this._pdfs_counter)
        delete(this._email)
        
        this.logger.info('Stuff closed...')
    },

    // _authenticate : async function(email, passwd) {
    //   await this._open_login_page() 
    //   if (! GoogleSearch._is_user_authenticated(email)) {
    //     await this._fill_in_email(email)
    //     await this._fill_in_password(passwd)    
    //   }
    // },

    get_search_results : async function(email, password, q) {

      this.logger.info('---------------------------------------------------')
      this.logger.info(`Searching for ${q} with ${email}`)

      try {
        await this._login(email, password) 
        await this._open_google_search()
        await this._fill_in_search_string(q)
        return await this._get_results(q)
      } catch(e) {
        let msg = `${email} failed to search for '${q}': ${e}`
        this.logger.error(msg)
        throw msg
      }

      // Ta dando pau pq eu nao passei o email. 
      // Criar teste pra isso
      // Refatorar screenshots para capturar telas de cada chamada
    },

    _screenshot: async function(description) {

      this.logger.debug(`Screeshotting \'${description}\'`)

      const fullpath = this._get_fullpath('screenshot', description, 'png')
      this.logger.silly(`Saving screenshot at ${fullpath}`)
      try {
        await this.page.screenshot({path: fullpath})
        this.logger.silly('\''+description+'\' screenshotted')
      } catch(e) {
        this.logger.warn('\''+description+'\' screenshot failed:' + e)
        this.logger.warn('Trying again...')
        try {
          await this.page.screenshot({path: fullpath});
        } catch(e2) {
          this.logger.warn('\''+description+'\' screenshot failed AGAIN:' + e)
          this.logger.warn('Givin\'up.')
          throw e2
        }
      }
      
    },

    _savepage: async function(description) {
      
      this.logger.debug(`Saving html of \'${description}\'`)

      const fullpath = this._get_fullpath('html', description, 'html')
      this.logger.silly(`Saving html page of ${description} at ${fullpath}`)

      // const urls = new URL(this.page.url());
      // let filePath = path.resolve(`./output${urls.pathname}`);
      // if (path.extname(urls.pathname).trim() === '') {
      //   filePath = `${filePath}/index.html`;
      // }
      //await fse.outputFile(filePath, await response.buffer());
      await fse.outputFile(fullpath, await this.page.content()).then(
        success => {
          this.logger.silly(`${description} html saved at ${fullpath}`)
        },
        failure => {
          let msg = `Failure saving html page of ${description}: ${failure}`
          this.logger.warn(msg) // TODO Error ou Warn?
          throw msg
        }
      )
    },

    _saveaspdf: async function(description) {

      this.logger.debug(`Saving page as PDF \'${description} \'`)

      const fullpath = this._get_fullpath('pdf', description, 'pdf')
      this.logger.silly(`Saving page PDF at ${fullpath}`)

      try {
        await this.page.pdf({path: fullpath})
      } catch (e) {
        let msg = `Error generating ${description} page PDF: ${e}`
        this.logger.warn(msg) // TODO Error ou Warn?
        throw msg
      }
      
    },

    _get_root_path: function() {
      
      // console.log('get_root_path')

      var root_path = `./output/${this._get_session_timestamp()}-${this._context}`

      if (!fs.existsSync(root_path)){
          fs.mkdirSync(root_path);
          fs.mkdirSync(`${root_path}/screenshot`); //TODO Criar dinamicamente a partir de uma lista de tipos de artefatos?
          fs.mkdirSync(`${root_path}/html`);
          fs.mkdirSync(`${root_path}/pdf`);
          fs.mkdirSync(`${root_path}/log`);
      }

      return root_path
    },

    _get_fullpath: function(artifact_type, description, extension) {
      
      this.logger.silly('_get_fullpath')

      const path = `${this._get_root_path()}/${artifact_type}` //TODO Testar se existe
      const timestamp = this._get_session_timestamp()
      const counter = this._get_counter(artifact_type)
      let account = 'unknown'

      if (this._email && typeof this._email != 'undefined') {
        account = this._email.slice(0,this._email.indexOf('@'))
        this.logger.silly(`Email set. Account=${account}`)
      } else {
        this.logger.silly(`Email not set. Account=${account}`)
      }
      
      // console.log('2')
      // Uglyness to avoid one more function argument
      // let re = /.*\n.*\n *at (.*) .*\n/g
      // let re = /\[as (.*)\]/g
      let re1 = /\[as (.*)\]/g
      let re2 = /.*\/(.*\.js:\d+:\d+)/g
      //let re = /(\[as (.*)\]|.*\/(.*\.js:\d+:\d+))/g
      let stack = new Error().stack;
      // console.log('3')
      // console.log('--------->')
      // console.log(stack)
      // console.log('<---------')

      // Error:
      //   at Object._get_fullpath (/Users/alegomes/GDrive/2019/code/google-search-scraper/main.js:475:19)
      //   at Object._get_fullpath [as _screenshot] (/Users/alegomes/GDrive/2019/code/google-search-scraper/main.js:396:29)
      //   at Object._screenshot [as _get_results] (/Users/alegomes/GDrive/2019/code/google-search-scraper/main.js:201:18)
      //   at Object._get_results (/Users/alegomes/GDrive/2019/code/google-search-scraper/main.test.js:148:36)
      //   at /Users/alegomes/GDrive/2019/code/google-search-scraper/node_modules/jest-jasmine2/build/queueRunner.js:43:12
      //   at new Promise (<anonymous>)
      //   at mapper (/Users/alegomes/GDrive/2019/code/google-search-scraper/node_modules/jest-jasmine2/build/queueRunner.js:26:19)
      //   at /Users/alegomes/GDrive/2019/code/google-search-scraper/node_modules/jest-jasmine2/build/queueRunner.js:73:41
      //  at processTicksAndRejections (internal/process/task_queues.js:85:5)
      
      // let match = re.exec(stack);
      // let thecaller = match[2]

      let thecaller = 'unknown'

      // let match = Array.from(stack.matchAll(re1)) # Nao funciona no Node 10x
      let match = Array.from(matchAll(stack, re1))
      if (match.length > 0) {
        // console.log('4')
        if (match[1]) {
          thecaller = match[1][1]
        } 
      } else {
        // console.log('5')
        // match = Array.from(stack.matchAll(re2))
        match = Array.from(matchAll(stack,re2))
        thecaller = match[1][1].replace(/\./g,'')
      } 
      // console.log('6')
      // const caller = this._screenshot.caller.name
      const fullpath = path + '/' + timestamp + '.' + counter + '.' + account + '.' + thecaller + '.' + description + '.' + extension
      // console.log('7')
      return fullpath
    },

    _get_browser: async function() {

      this.logger.silly('_get_browser')

      if (typeof this.browser == 'undefined' || this.browser == null || ! this.browser.isConnected()) {

        try {
          this.logger.silly('Browser launching...');
          this.browser = await puppeteer.launch(
            // Este primeiro parametro dispara o erro:
            // (node:1508) UnhandledPromiseRejectionWarning: Error: Failed to launch chrome!
            // [0930/142141.167540:ERROR:zygote_host_impl_linux.cc(89)] Running as root without --no-sandbox is not supported. See https://crbug.com/638180.
            { 
              // headless: false,
              defaultViewport: { width: 1440, height: 900},
              dumpio: true,
              // devtools: true
            //},
            //{
              //https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#puppeteerlaunchoptions
              args: [

                // Required for Docker version of Puppeteer
                // https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md#setting-up-chrome-linux-sandbox
                '--no-sandbox',
                '--disable-setuid-sandbox',

                // This will write shared memory files into /tmp instead of /dev/shm,
                // because Docker’s default for /dev/shm is 64MB
                '--disable-dev-shm-usage'
              ]
            }) 
        } catch (e) {
          this.logger.error('Error creating browser instance: ' + e);
          throw e;
        }
        this.logger.silly('Returning brand new browser object: ' + this.browser)       
      } else {
        this.logger.silly('Returning existing browser object: ' + this.browser)
      }

      return this.browser

    },

    _get_page: async function() {

      this.logger.silly('_get_page')

      this.browser = await this._get_browser()

      if (typeof this.page == 'undefined' || this.page == null || this.page.isClosed()) {
        try {
          this.logger.silly('Opening new page...');
          this.page = await this.browser.newPage();
          this.page.on('console', msg => {
            // for (let i = 0; i < msg.args().length; ++i) {
              // this.logger.silly('[console] ' + msg.args()[i])
            // }
            this.logger.silly('[console] ' + msg)
          });

          this.page.on('error', e => {
              this.logger.error('[console] ' + e)
          });

          this.page.on('pageerror', e => {
            this.logger.error('[console] ' + e)
          });

          this.page.on('load', e => {
            this.logger.silly('[console] ' + e)
          });

          this.page.on('domcontentloaded', e => {
            this.logger.silly('[console] ' + e)
          });

      
          // await this.page.setViewport({
          //     width: 1440, //1680
          //     height: 900, //1050
          //     deviceScaleFactor: 1,
          //   });

          // page.setJavaScriptEnabled(enabled)
        } catch (e) {
          this.logger.error('Error opening new page: ' + e)
          throw e
        }
        this.logger.silly('Retrieving a new page: ' + this.page)

      } else {
        this.logger.silly('Retrieving current page: ' + this.page)
      }

      return this.page
    },

    _get_counter: function(artifact_type) {
     
      this.logger.silly('_get_counter')

      if(artifact_type == 'screenshot') {
        return this._get_screenshot_counter()
      } else if (artifact_type == 'html') {
        return this._get_html_counter()
      } else if (artifact_type == 'pdf') {
        return this._get_pdf_counter()
      }
    },

    _get_screenshot_counter: function() {
      if (typeof this._screenshots_counter == 'undefined') {
        this._screenshots_counter = 1
      } else {
        ++this._screenshots_counter
      }

      return this._screenshots_counter
    },

    _get_html_counter: function() {
      if (typeof this._htmls_counter == 'undefined') {
        this._htmls_counter = 1
      } else {
        ++this._htmls_counter
      }

      return this._htmls_counter
    },

    _get_pdf_counter: function() {
      if (typeof this._pdfs_counter == 'undefined') {
        this._pdfs_counter = 1
      } else {
        ++this._pdfs_counter
      }

      return this._pdfs_counter
    },

    _get_session_timestamp: function() {
      if (typeof this._timestamp == 'undefined') {
        const tzoffset = (new Date()).getTimezoneOffset() * 60000;
        this.timestamp_fmt = (new Date(Date.now() - tzoffset)).toISOString().substring(0,19)
        this._timestamp = this.timestamp_fmt.replace(/[T\-\:]/g,'');
      }
      return this._timestamp
    },

    _setup: function(context) {
      this._context = context // scrape, test, validation

      const output_folder = GoogleSearch._get_root_path()
      const started_at = GoogleSearch._get_session_timestamp()

      const myFormat = winston.format.printf(({ level, message, label, timestamp }) => {
        // return `${timestamp} [${level}] ${label}: ${message}`;
        if(label) {
          return `${timestamp} [${level}] {${label}} ${message}`;
        } else {
          return `${timestamp} [${level}] ${message}`;
        }
        
      });

      this.logger = winston.createLogger({
        level: 'info',
        // format: winston.format.json(),
        format: winston.format.combine(
          winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
          }),
          winston.format.errors({ stack: true }),
          winston.format.splat(),
          winston.format.json(),
          // winston.format.colorize(),
          myFormat
          // winston.format.label({ label: 'right meow!' }),
          // winston.format.timestamp(),
          // winston.format.prettyPrint()
        ),
        defaultMeta: { service: 'google-search-scraper' },
        transports: [
          // https://github.com/winstonjs/winston#logging-levels
          new winston.transports.File({ filename: `${output_folder}/log/${started_at}-5.silly.log`, level: 'silly' }),
          new winston.transports.File({ filename: `${output_folder}/log/${started_at}-4.debug.log`, level: 'debug' }),
          new winston.transports.File({ filename: `${output_folder}/log/${started_at}-3.verbose.log`, level: 'verbose' }),
          new winston.transports.File({ filename: `${output_folder}/log/${started_at}-2.info.log`, level: 'info' }),
          new winston.transports.File({ filename: `${output_folder}/log/${started_at}-1.warn.log`, level: 'warn' }),
          new winston.transports.File({ filename: `${output_folder}/log/${started_at}-0.error.log`, level: 'error' }),
          // new winston.transports.File({ filename: `${output_folder}/log/combined-${started_at}.log`, level: 'silly' }),
          // new winston.transports.Console()
        ]
      });

      this._get_session_timestamp()
      
      //
      // If we're not in production then log to the `console` with the format:
      // `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
      // 
      // if (process.env.NODE_ENV !== 'production') {
      //   this.logger.add(new winston.transports.Console({
      //     format: winston.format.simple()
      //   }));
      // }

    },

    _is_user_authenticated: async function(email) {

      let ctx = {label : '_is_user_authenticated'}

      this.logger.verbose(`Is ${email} authenticated?`, ctx)

      this._email = email

      try {
        // 3 coisas podem acontacer aqui:
        //   - usuario eh autenticado h1.x7WrMb
        //   - pagina de verificacao da conta pelo celular (#challengePickerList)
        //   - captcha para submissao da senha
        // await this.page.waitForNavigation({timeout : 0, waitUntil: ['load']}).then(
        // await this.page.waitForXPath(`//h1[contains(text(),"${GooglsSearch._fullname}")]`).then(
        // await this.page.waitForSelector('h1#x7WrMb') // Era ID e virou classe!!!
        await this.page.waitFor('//h1[contains(text(),"Bem-vindo")]')
        this.logger.verbose('Done. User seems to be authenticated.', ctx);
        
        await this._screenshot('signed_in');
        await this._savepage('signed_in')

        return true
      } catch (e) {
        this.logger.debug(`User ${email} not authenticated. "Bem-vindo" not found: ${e}`, ctx)
      }

      try {
        this.logger.debug('Looking for "Olá" instead', ctx)

        await this.page.waitFor('//h1[contains(text(),"Olá")]')

        this.logger.debug('"Olá" found. User seems to be authenticated.', ctx);
        
        await this._screenshot('signed_in');
        await this._savepage('signed_in')

        this._email = email

        this.logger.verbose(`User ${email} authenticated.`)
        return true
      } catch (e) {
        this.logger.debug(`"Olá" not found as well.`, ctx)
        // this.logger.error(`User ${email} not authenticated. Check "authentication_failed" screenshot.`)
        this.logger.verbose(`User ${email} not authenticated. Check "authentication_failed" screenshot. See root cause below:`, ctx)
        this.logger.debug(e, ctx)

        //E se e = Protocol error (Runtime.callFunctionOn): Session closed. Most likely the page has been closed. ?

        await this._screenshot('authentication_failed');
        await this._savepage('authentication_failed')  

        return false
      }
    }, 

    _check_if_suspicious: async function() {

      let ctx = {label : 'suspicious'}

      try {
        this.logger.debug('Suspicious activity?', ctx); // TODO
        // await this.page.waitFor('//h1[contains(text(),"Alterar senha")]')
        await this.page.waitFor('#suspicious-activity');
        this.logger.debug('Suspicious activity identified. Password needs to be changed.', ctx);
      }
      catch (e1) {
        this.logger.debug('Doesn\'t seem to be a suspucious activity: ' + e1, ctx);
        return
      }
      this._screenshot('suspicious_activity');
      this._savepage('suspicious_activity');
      throw `Suspicious activity identified at ${this._email} account. Password needs to be change.`;
    }

}


module.exports = GoogleSearch;


if (typeof require != 'undefined' && require.main==module) {

    GoogleSearch._setup('scraper')
    console.log(`Saving artifacts at ${GoogleSearch._get_root_path()}`)

    // let pg = require('knex')(development);
    const knex = require('./knex/knex.js');
    //   {
    //   client: 'pg',
    //   // connection: process.env.DATABASE_URL,
    //   //searchPath: ['knex', 'public'],
    //   connection: {
    //     host : '127.0.0.1',
    //     user : 'postgres',
    //     password : 'resocie123',
    //     database : 'googlescraper'
    //   }
    // });

    const CREDS = require('./input/credentials');
    const TERMS = './input/terms.txt';
    const OUTPUT = './output/database.csv';

    let terms_as_str = fs.readFileSync(TERMS, 'utf8');
    let terms = terms_as_str.split('\n');
    let qtd_termos = terms.length;

    (async () => {
      GoogleSearch.logger.debug('Starting to iterate over searching terms...')
      for(var i = 0; i < qtd_termos; i++) {
        let k = Math.floor(Math.random()*terms.length)
        let term = terms.splice(k,1)[0]
        GoogleSearch.logger.debug('Searching term chosen randomly: ' + term)

        let users = [...Array(16).keys()].map(x => `user${x}`)
        let qtd_users = users.length

        GoogleSearch.logger.debug('Starting to iterate over users accounts')
        for(var j = 0; j < qtd_users; j++) {
          let l = Math.floor(Math.random()*users.length)
          let user = users.splice(l,1)[0]

          GoogleSearch.logger.debug('User randomly chosen: ' + user)
          process.stdout.write(`Randomly chose ${term} + ${user}. Let's run it? `)
          if (CREDS[user]) { // Pode ser que o usuario esteja comentado no arquivo de credenciais
            await console.log(' - YES')
            await console.log(`Searching '${term}' with ${user} (${CREDS[user].email})`)

            // let email = CREDS['user0'].email
            // let passwd = CREDS['user0'].passwd
            // let query = 'Alberto Fernández'
            
            let email = CREDS[user].email
            let passwd = CREDS[user].passwd
            let query = term
            
            try {
              GoogleSearch.logger.debug(`${email} about to search for ${query}`)
              let results = await GoogleSearch.get_search_results(email, passwd, query)
              GoogleSearch.logger.debug(`${email} searched for ${query} and found ${results.length} results`)
              
              GoogleSearch.logger.info(`Saving ${results.length} results to database`)
              knex('search_results').insert(results)
                .then(() => {
                  console.log("Results inserted to database")
                  GoogleSearch.logger.info(`Results saved in the database`)
                })
                .catch((err) => { console.log(err); throw err })
            } catch (e) {
              let msg = "Search failed:" + e
               console.log(msg)
              GoogleSearch.logger.error(msg)
              
              
              var error = {
                'timestamp' : GoogleSearch._timestamp_fmt,
                'user' : CREDS[user].email.split('@')[0],
                'query' : query,
                'position' : 0,
                'title' : 'ERROR',
                'description' : e,
                'url' : '',
                // 'created_at': ,
                // 'updated_at': 
              }

              GoogleSearch.logger.info(`Saving search error to database`)
              knex('search_results').insert(error)
                .then(() => {
                  console.log("Search error inserted to database")
                  GoogleSearch.logger.info(`Search error saved in the database`)
                })
                .catch((err) => { console.log(err); throw err })

            }
            
              // .finally(() => {
              //     knex.destroy();
              // });
            
            // GoogleSearch.logger.info(`Saving results to CSV file...`)
            // results.forEach((result) => {
      
            //   let timestamp = GoogleSearch._get_session_timestamp()
            //   let user = email
            //   let position = result['position']
            //   let title = result['title']
            //   let description = result['description']
            //   let url = result['url']
      
            //   let line = `${timestamp};${query};${user};${position};${url};${title};${description}\n`
            //   fs.appendFile(OUTPUT, line, function(err) {
      
            //     if(err) {
            //         return console.log('[ERROR] '+ err);
            //     }
            
              // });  // writeFile
      
            // }) // forEach

            // GoogleSearch.logger.info(`Results saved to CSV file...`)

          } // if CREDS[user] 
          else {
            console.log(' - NO')
          }
          
        } // for user
        
      }
  
      GoogleSearch.logger.debug('Closing database')
      knex.destroy();
      GoogleSearch.logger.debug('Database closed')
    })().then( result => { 
      console.log('Fim: ' + result)
    }).catch( reason => {
      console.log("Erro: " + reason)
    }).finally( () => {
      GoogleSearch.close();
    });
    
}
