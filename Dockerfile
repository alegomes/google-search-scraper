# FOLLOW UP
# docker container ls -a
# docker logs <container id>

# CLEAN UP
# docker container stop $(docker container ls -a -q)
# docker container rm $(docker container ls -a -q)
# docker image rm -f $(docker image ls -a -q)

# BUILD 
# docker build -t alegomes/google-search-scraper .

# RUN
# docker run  --mount type=bind,source=/Users/alegomes/GDrive/2019/code/google-search-scraper/output/,target=/scraper/output -d alegomes/google-search-scraper
# docker run  --mount type=bind,source=/home/linuxadmin/output/,target=/scraper/output -d alegomes/google-search-scraper --restart=always
# docker run  --mount type=bind,source=/home/linuxadmin/output/,target=/scraper/output -it  alegomes/google-search-scraper /bin/bash
# docker exec -it <container id> /bin/bash
# docker run --name google -d alegomes/google-search-scraper
# docker run --name google -d alegomes/google-search-scraper sh -c "while true; do $(echo date); sleep 1; done"
# docker run -it docker-puppeteer-ale  /bin/bash
# docker run  --mount type=bind,source=/tmp,target=/scraper/screenshots -d docker-puppeteer-ale
# docker run  --mount type=bind,source=/tmp,target=/scraper/screenshots -d alegomes/google-search-scraper
# docker run  --mount type=bind,source=output,target=/scraper/output -d alegomes/google-search-scraper
# docker run  --mount type=bind,source=/Users/alegomes/GDrive/2019/code/google-search-scraper/output/,target=/scraper/output -d alegomes/google-search-scraper
# docker run -it alegomes/google-search-scraper  /bin/bash
# docker run -u 0 --mount type=bind,source=/Users/alegomes/GDrive/2019/code/google-search-scraper/output/,target=/scraper/output -it alegomes/google-search-scraper  /bin/bash
## docker run --shm-size 1G -u 0 --mount type=bind,source=/Users/alegomes/GDrive/2019/code/google-search-scraper/output/,target=/scraper/output -it alegomes/google-search-scraper  /bin/bash

FROM buildkite/puppeteer:latest

#COPY --chown=node:node . /scraper

ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir /scraper
COPY package.json /scraper
COPY main.js /scraper
COPY test_accounts.js /scraper
COPY validate_accounts.js /scraper
COPY knex /scraper/knex
COPY knexfile.js /scraper
#COPY input /scraper/input
RUN mkdir -p /scraper/input
RUN mkdir -p /scraper/output
RUN chown -R node:node /scraper

RUN apt-get update; apt-get -y install vim cron

ADD crontab /etc/cron.d/simple-cron

ADD main.sh /scraper/main.sh
RUN chmod +x /scraper/main.sh
RUN chmod 0644 /etc/cron.d/simple-cron
RUN touch /var/log/cron.log
RUN chown node:node /var/log/cron.log

USER node
WORKDIR /scraper
RUN npm i 

USER root

# CMD [ "npm", "start" ]
CMD cron && tail -f /var/log/cron.log

