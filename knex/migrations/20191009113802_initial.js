// https://devhints.io/knex
//   npm install knex --save
//   npm install pg --save
//   knex init
//   ./node_modules/.bin/knex init

// http://knexjs.org/#Migrations
//   ./node_modules/.bin/knex migrate:make initial
//   npm install sqlite3 --save
//   ./node_modules/.bin/knex migrate:latest
//   knex migrate:latest --env production
//   NODE_ENV=production knex migrate:latest
  
exports.up = function(knex) {
    return knex.schema.createTable('search_results', (table) => {
        table.increments('id')
        table.timestamp('timestamp')
        table.string('user')
        table.string('query')
        table.string('position')
        table.string('title')
        table.text('description')
        table.string('url')
        table.text('error')
        table.timestamps(false, true)
      })
      .then(() => console.log('Table "search_results" created successfuly'))
};

exports.down = function(knex) {
    return knex.schema.dropTable("search_results")
};
