#!/bin/bash

TIMESTAMP=$(date +%Y%m%d%H%M)
export PATH=/usr/local/bin:$PATH
export NODE_ENV=production 
cd /scraper
./node_modules/.bin/knex migrate:latest > output/main.sh_${TIMESTAMP}.log
npm start > output/main.sh_${TIMESTAMP}.log
echo "$(date): main script" >> /var/log/cron.log 2>&1
