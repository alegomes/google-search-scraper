// https://jestjs.io/docs/en/getting-started.html
// https://github.com/smooth-code/jest-puppeteer/tree/master/packages/jest-environment-puppeteer#jest-puppeteerconfigjs
// yarn test or npm run test

const winston = require('winston')
const fs = require('fs')
const GoogleSearch = require('./main');
const AVATAR_SELECTOR = '#account-switcher-left > summary > div > img';
const CREDS = require('./input/credentials');
// credentials
// module.exports = {
//     email: 'alegomes@gmail.com',
//     passwd: 'blablabla'
// }

// jest.useFakeTimers();

// const sleep1 = (milliseconds) => {
//     return new Promise(resolve => setTimeout(resolve, milliseconds))
// }

// function sleep2(ms){
//     return new Promise(resolve=>{
//         setTimeout(resolve,ms)
//     })
// }

// const output_folder = GoogleSearch._get_root_path()
// const started_at = GoogleSearch._get_session_timestamp()

const myFormat = winston.format.printf(({ level, message, label, timestamp }) => {
    // return `${timestamp} [${level}] ${label}: ${message}`;
    return `${timestamp} [${level}] ${message}`;
});

const logger = winston.createLogger({
    level: 'silly',
    format: winston.format.combine(
    winston.format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }),
    winston.format.errors({ stack: true }),
    winston.format.splat(),
    winston.format.json(),
    winston.format.colorize(),
    myFormat
    ),
    defaultMeta: { service: 'google-search-scraper-test' },
    transports: [
    // https://github.com/winstonjs/winston#logging-levels
    // new winston.transports.File({ filename: `${output_folder}/log/silly-${started_at}.log`, level: 'silly' }),
    // new winston.transports.File({ filename: `${output_folder}/log/debug-${started_at}.log`, level: 'debug' }),
    // new winston.transports.File({ filename: `${output_folder}/log/verbose-${started_at}.log`, level: 'verbose' }),
    // new winston.transports.File({ filename: `${output_folder}/log/info-${started_at}.log`, level: 'info' }),
    // new winston.transports.File({ filename: `${output_folder}/log/warn.-${started_at}.log`, level: 'warn' }),
    // new winston.transports.File({ filename: `${output_folder}/log/error-${started_at}.log`, level: 'error' }),
    // new winston.transports.File({ filename: `${output_folder}/log/combined-${started_at}.log`, level: 'silly' }),
    new winston.transports.Console()
    ]
});

describe('Google search scraper', () => {
    
    beforeAll((done) => {
        GoogleSearch.logger.debug('[test] beforeAll Google Search Scraper');
        jest.setTimeout(240000);
        done();
    });

    afterAll(async (done) =>  {
        await GoogleSearch.logger.debug('[test] afterAll Google Search Scraper');
        await GoogleSearch.close();
        done();
    });

    describe('Running preparation', () => {

        beforeAll(async (done) => {
            GoogleSearch.logger.debug('[test] beforeAll Running Preparation')
            await GoogleSearch.close()
            done();
        });

        afterAll(async (done) => {
            GoogleSearch.logger.debug('[test] afterAll Running Preparation')
            await GoogleSearch.close()
            done();
        });

        test('should count artifacts independently', async (done) => {
            logger.info('it should count artifacts independently')

            var artifact = ['screenshot', 'html']
            expect(GoogleSearch._get_counter(artifact[0])).toBe(1)
            expect(GoogleSearch._get_counter(artifact[0])).toBe(2)
            expect(GoogleSearch._get_counter(artifact[0])).toBe(3)

            expect(GoogleSearch._get_counter(artifact[1])).toBe(1)
            expect(GoogleSearch._get_counter(artifact[1])).toBe(2)
            expect(GoogleSearch._get_counter(artifact[1])).toBe(3)

            expect(GoogleSearch._get_counter(artifact[0])).toBe(4)
            expect(GoogleSearch._get_counter(artifact[1])).toBe(4)
            expect(GoogleSearch._get_counter(artifact[1])).toBe(5)
            expect(GoogleSearch._get_counter(artifact[0])).toBe(5)

            delete(GoogleSearch._screenshots_counter)
            delete(GoogleSearch._htmls_counter)
            delete(GoogleSearch._pdfs_counter)

            done();
            
        });

        test('should create output dir', async (done) => {
            logger.info('it should create output dir')
            try {
                await GoogleSearch._open_login_page();
            } catch (e) {
                logger.debug('Error opening login page to verify if output dir is being created: ' + e)
            }

            let path = GoogleSearch._get_root_path()
            logger.debug(`Checking if ${path} exists...`)
            await expect(fs.existsSync(path)).toBeTruthy()
            logger.debug(`${path} exists.`)

            done();
        })

        test('should track original caller', async (done) => {
            logger.info('it should track original caller')
            //TODO Como testar o funcionamento da outra regex?

            let myFunc = () => {
                return GoogleSearch._get_fullpath('screenshot', 'caller_tracking', 'wtv')
            }
            // let path = myFunc()
            let path = GoogleSearch._get_fullpath('screenshot', 'caller_tracking', 'wtv')
            expect(path).toMatch(/maintestjs.*caller_tracking/)
            done();
        })

        test('should track account name', async (done) => {
            logger.info('it should track account name')

            let email = 'resocie.gov@gmail.com'
            let passwd = 'resocieargentina'

            // Expect com prazo de validade
            try {
                await GoogleSearch._login(email, passwd)
            } catch (e) {
                //expect(e).toMatch(/Suspicious/)
                throw e
            }

            let path = await GoogleSearch._get_fullpath('screenshot', 'caller_tracking', 'wtv')
            expect(path).toMatch(/screenshot.*caller_tracking.*wtv/)
            expect(path).toMatch(/resocie\.gov/)
            done();
        })

        describe('Screenshots', () => {

            beforeAll(async () => {
                logger.debug('beforeAll Screenshots')
                await GoogleSearch.close()
            });
    
            test('should save screenshots', async (done) => {
                logger.info('it should save screenshots')
    
                // About mocks: https://medium.com/@rickhanlonii/understanding-jest-mocks-f0046c68e53c
                var screenshot = jest.spyOn(GoogleSearch, '_screenshot')
                var get_fullpath = jest.spyOn(GoogleSearch, '_get_fullpath')
    
                // let watcher = jest.fn((t,f) => {
                //     screenshotted(t,f)
                // })
    
                let path = await GoogleSearch._get_root_path() 
                // await fs.watch(path, watcher);
                
                await logger.debug('Opening login page')
                await GoogleSearch._open_login_page();
    
                // await GoogleSearch.page.waitFor(5000)
    
                // await expect(watcher).toBeCalled()
                await expect(screenshot).toBeCalled()
                await expect(get_fullpath).toBeCalled()
                await expect(get_fullpath).toHaveBeenCalledWith('screenshot', 'login_page', 'png')
                
                // await expect(fs.existsSync(path)).toBeTruthy()
    
                done();
            })
        }) // screenshots

    }); // Running preparation

    describe('Google authentication', () => {

        describe('step by step', () => {

            beforeAll(async (done) => {
                GoogleSearch.logger.debug('[test] beforeAll step by step')
                await GoogleSearch.close()
                done();
            });
    
            afterAll(async (done) => {
                GoogleSearch.logger.debug('[test] afterAll step by step')
                await GoogleSearch.close()
                done();
            });
    
            test('should open login page successfuly', async (done) => {
                logger.info('it should open login page successfuly')
                await GoogleSearch._open_login_page();
                
                try {
                    await expect(GoogleSearch.page).toMatchElement('#identifierId'); // TODO Parametrizazr o seletor
                } catch (e) {
                    logger.debug(e);
                    logger.debug('Asserting #Email...')
                    await expect(GoogleSearch.page).toMatchElement('#Email');
                }
                
                done();
            });
    
            test('should fill in email', async (done) => {
                logger.info('it should fill in email')
                const email = CREDS.user0.email;
    
                await GoogleSearch._fill_in_email(email);
                await expect(GoogleSearch.page).toMatchElement('#Passwd');
                done();
            });
    
            test('should fill in password', async (done) => {
                logger.info('it should fill in password')
                const passwd = CREDS.user0.passwd;
    
                try {
                    await GoogleSearch._fill_in_password(passwd);
                    await expect(GoogleSearch.page).toMatch('Bem-vindo, Alexandre Gomes');
                } catch (e) {
                    logger.debug('Error filling password: ' + e);
                }
                
                done();
            });
    
            test('should open authenticated google search page', async (done) => {
                logger.info('it should open authenticated google search page')
                await GoogleSearch._open_google_search();
                await expect(GoogleSearch.page).toMatch(CREDS.user0.email);
                done();
            });

        }) // step by step

        // describe('single step', () => {

        //     beforeEach(async (done) => {
        //         GoogleSearch.logger.debug('[test] beforeAll authenticate user')
        //         await GoogleSearch.close()
        //         done();
        //     });

        //     test('should authenticate user', async (done) => {
        //         logger.info('it should authenticate user')
        //         await GoogleSearch._authenticate(CREDS.user0.email, CREDS.user0.passwd)
        //         await GoogleSearch._open_google_search();
        //         await expect(GoogleSearch.page).toMatch(CREDS.user0.email);
        //         done();

        //     });

        //     test.only('should authenticate more than once with no failure', async (done) => {
        //         logger.info('should authenticate more than once with no failure')
        //         await GoogleSearch.logger.info('\n***** 1st auth')
        //         await GoogleSearch._authenticate(CREDS.user0.email, CREDS.user0.passwd)
        //         // await GoogleSearch.logger.info('\n***** 2st auth')
        //         // await GoogleSearch._authenticate(CREDS.user0.email, CREDS.user0.passwd)
        //         // await GoogleSearch.logger.info('\n***** 3st auth')
        //         // await GoogleSearch._authenticate(CREDS.user0.email, CREDS.user0.passwd)
        //         // await GoogleSearch.logger.info('\n***** opening google search page')
        //         // await GoogleSearch._open_google_search();
        //         await expect(GoogleSearch.page).toMatchElement('a[title*="alegomes"]');
        //         done();

        //     });

        // }) // single step

        describe('verify your account', () => {
            xtest('should revoke trusted device', async (done) => {
                logger.debug('it ')
                done();
            });

            xtest('should identify verification need', async (done) => {
                logger.info('it should identify verification need')
                let email = CREDS.user2.email
                let passwd = CREDS.user2.passwd
                let phone = CREDS.user2.phone

                expect(async () => {
                    await GoogleSearch._login(email, passwd, phone)
                  }).toThrow();

                // try {
                //     await GoogleSearch._login(email, passwd, phone)
                //     expect(GoogleSearch.page).toMatch('Confirme que é você')
                // } catch (e) {
                //     logger.error('Could not verify account: ' + e)
                //     logger.error(e.stack)
                //     expect(e).toMatch(/account temporarily blocked/i)
                // }
                
                done();
            });   
             
            xtest('should verify account', async (done) => {
                logger.debug('it ')
                done();
            });   
            
        }) // verify your account

        describe('single step', () => {

            beforeAll(async (done) => {
                GoogleSearch.logger.debug('[test] beforeAll single step')
                await GoogleSearch.close()
                done();
            });
    
            afterAll(async (done) => {
                GoogleSearch.logger.debug('[test] afterAll single step')
                await GoogleSearch.close()
                done();
            });

            test('should login in one single step', async (done) => {
                GoogleSearch.logger.info('should login in one single step')
                await GoogleSearch._login(CREDS.user0.email, CREDS.user0.passwd)
                await expect(GoogleSearch.page).toMatch(`Bem-vindo, ${CREDS.user0.fullname}`);
                GoogleSearch.logger.info('should login in one single step - Done')
                done();
            });

            test.only('should do multiple logins without failure', async (done) => {
                GoogleSearch.logger.info('it should do multiple logins without failure')
                await GoogleSearch._login(CREDS.user0.email, CREDS.user0.passwd)
                await GoogleSearch._login(CREDS.user0.email, CREDS.user0.passwd)
                await GoogleSearch._login(CREDS.user0.email, CREDS.user0.passwd)
                GoogleSearch.logger.info('[test] Expecting Bem-vindo')
                await expect(GoogleSearch.page).toMatch(`Bem-vindo, ${CREDS.user0.fullname}`);
                GoogleSearch.logger.info('it should do multiple logins withou failure - Done')
                done();
            });

        }); // single step

    }); // Google authentication

    describe('Google authenticated search', () => {

        beforeAll(async (done) => {
            // logger.debug('------------------------------------------------')
            GoogleSearch.logger.debug('[test] beforeAll Google authenticated search')
            await GoogleSearch.close()
            await GoogleSearch._login(CREDS.user0.email,CREDS.user0.passwd)
            await GoogleSearch._open_google_search();
            done();
        });

        afterAll(async (done) => {
            GoogleSearch.logger.debug('[test] afterAll Google authenticated search')
            await GoogleSearch.close()
            // logger.debug('------------------------------------------------')
            done();
        });

        describe('"private" methods', () => {

            beforeAll(async (done) => {
                // logger.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
                GoogleSearch.logger.debug('[test] beforeAll private methods')
                done();
            });
    
            afterAll(async (done) => {
                GoogleSearch.logger.debug('[test] afterAll private methods')
                // logger.debug('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<')
                done();
            });

            test('should fill in search term', async (done) => {
                logger.info('it should fill in search term')
                const SEARCH_TERM = 'guinness';
                const RESULT_STATS_SELECTOR = '#resultStats';

                await GoogleSearch._fill_in_search_string(SEARCH_TERM);
                await expect(GoogleSearch.page).toMatchElement(RESULT_STATS_SELECTOR); // Assuming javascript is disabled
                done();
            });

            test('should return minimum of results', async (done) => {
                logger.info('it should return minimum of results')
                await GoogleSearch._get_results().then((results) => {
                    const qtd = results.length
                    logger.debug('results_length=' + qtd)
                    expect(qtd).toBeGreaterThan(5) 
                    done()
                })
            });

            // TODO It may vary over time...
            // test('should have exactaly 13 results', async (done) => {
            //     await GoogleSearch._get_results().then((results) => {
            //         const qtd = results.length
            //         logger.debug('results_length=' + qtd)
            //         expect(qtd).toBe(13) 
            //         done()
            //     })
            // });

            // TODO Assert more details
            // TODO It may vary over time...
            test('should scrape results data corretly', async (done) => {
                logger.info('it should scrape results data corretly')
                await GoogleSearch._get_results().then((results) => {
                    expect(results[0].title).toMatch('Guinness World Records: Home')
                    expect(results[1].title).toMatch(/Recordes.*/)
                    // expect(results[2].title).toMatch(/.*Wiki.*/)
                    // expect(results[4].title).toMatch(/Veja os principais.*/)   //Casos de borda
                    // expect(results[5].title).toMatch(/10 recordes recentes.*/)
                    // expect(results[6].title).toMatch(/Os 15 recordes mais bizarros.*/)

                    expect(results[0].description).toMatch(/The Guinness World Records official site.*/)
                    expect(results[1].description).toMatch(/Explore os títulos de recorde.*/)
                    // expect(results[2].description).toMatch(/.*é uma edição publicada anualmente.*/)

                    expect(results[0].url).toMatch('https://www.guinnessworldrecords.com.br/')
                    expect(results[1].url).toMatch('https://www.guinnessworldrecords.com.br/records/')
                    // expect(results[2].url).toMatch('https://pt.wikipedia.org/wiki/Guinness_World_Records')
                })
                done()
            })

            test('should have max of 1 sponsored result', async (done) => {
                logger.info('it should have max of 1 sponsored result')
                await GoogleSearch._get_sponsored().then((sponsored) => {
                    expect(sponsored).not.toBeNull()
                    expect(sponsored.length).toBeLessThanOrEqual(1)
                    logger.debug(`${sponsored.length} sponsored results found`)
                    logger.debug('Sponsored results: '+ sponsored)
                })
                done()
            });

            test('should have 10 images result', async (done) => {
                logger.info('it should have 10 images result')
                await GoogleSearch._get_images().then((images) => {
                    expect(images).not.toBeNull()
                    expect(images.length).toBe(10)
                })
                done()
            });

            xtest('should have knowledge graph result', async (done) => {
                logger.info('it should have knowledge graph result')
                done()
            });

            xtest('should have people also ask', async (done) => {
                logger.info('it should have people also ask')
                done()
            });

            xtest('should have related searches', async (done) => {
                logger.info('it should have related searches')
                done()
            });

            xtest('should have related results', async (done) => {
                logger.info('it should have related results')
                done()
            });

            xtest('should have a location', async (done) => {
                logger.info('it should have a location')
                done()
            });

        });

        describe('"public" methods', () => {

            xtest('should load and parse the entire result page', async (done) => {
                logger.info('it should have a location')
                done()
            })

            test('should load correct results in one only call', async (done) => {
                logger.info('it should load correct results in one only call')
                // Make sure there is no previous browser window opened.
                await GoogleSearch.close()
                //TODO What if email or password is no valid?
                await GoogleSearch.get_search_results(CREDS.user0.email, CREDS.user0.passwd, 'guinness').then(
                    (results) => {
                        expect(results.length).toBeGreaterThan(5)
                        expect(results[0].title).toMatch('Guinness World Records: Home')
                        expect(results[1].title).toMatch(/Recordes.*/)
                        // expect(results[2].title).toMatch(/.*Wiki.*/)
                        // expect(results[4].title).toMatch(/Veja os principais.*/)   //Casos de borda
                        // expect(results[5].title).toMatch(/10 recordes recentes.*/)
                        // expect(results[6].title).toMatch(/Os 15 recordes mais bizarros.*/)

                        expect(results[0].description).toMatch(/The Guinness World Records official site.*/)
                        expect(results[1].description).toMatch(/Explore os títulos de recorde.*/)
                        // expect(results[2].description).toMatch(/.*é uma edição publicada anualmente.*/)

                        expect(results[0].url).toMatch('https://www.guinnessworldrecords.com.br/')
                        expect(results[1].url).toMatch('https://www.guinnessworldrecords.com.br/records/')
                        // expect(results[2].url).toMatch('https://pt.wikipedia.org/wiki/Guinness_World_Records')
                    }
                )
                done();
            });    
            
        });

    }); // Google authenticated search

    describe('Multiple searches', () => {

        beforeAll(() => {
            GoogleSearch.logger.debug('[test] beforeAll multiple queries multiple users')
        })

        afterAll(() => {
            GoogleSearch.logger.debug('[test] afterAll multiple searches')
        })

        let users = {
            user0 : CREDS.user0, 
            // user2 : CREDS.user2,  // to be verified
            user3 : CREDS.user3,
            user4 : CREDS.user4,
            user5 : CREDS.user5,
            user6 : CREDS.user6,
            user12 : CREDS.user12,
            user13 : CREDS.user13,
            user14 : CREDS.user14}

        // let credentials = ((users) => (users))(CREDS);
        let userstable = Object.values(users).map(
            (v,i,a) => { 
                return [v['email'],
                        v['passwd'],
                        v['phone'],
                        v['fullname'],
                        v['queries']] 
            })

        describe('single query multiple users', () => {

            afterAll(() => {
                GoogleSearch.logger.debug('[test] afterAll single query multiple users')
            })
             
            // test('should search same query for multiple users', async (done) => {
            // let t = [
            //     [ 'alegomes@gmail.com', '!2#Pipoc@', '+5561981338579', 'Alexandre Gomes', [ 'capoeira', 'samba', 'natacao' ]],
            //     [ 'resocie.gov3@gmail.com', 'resocieargentina', '+5561993338082', 'resocie governo3', [ 'computador', 'impressora', 'mouse' ]],
            //     [ 'resocie.gov4@gmail.com', 'resocieargentina', '+5561993338082', 'resocie governo4', [ 'arroz', 'feijao', 'abobora' ]],
            //     [ 'resocie.gov5@gmail.com', 'resocieargentina', '61 991815993', 'resocie governo5', [ 'macarrao', 'quiabo', 'bolo' ]],
            //     [ 'resocie.opo@gmail.com', 'resocieargentina', '61 991815993', 'resocie opo', [ 'oculos', 'bolsa', 'mochila' ]],
            //     [ 'resocie.neutro2@gmail.com', 'resocieargentina', '61 981338579', 'resocie neutro2', [ 'tecnologia', 'spam', 'democracia' ]],
            //     [ 'resocie.neutro3@gmail.com', 'resocieargentina', '61 984167562', 'resocie neutro3', [ 'comida', 'musica', 'lazer' ]],
            //     [ 'resocie.neutro4@gmail.com', 'resocieargentina', '61 984167562', 'resocie neutro4', [ 'maca', 'uva', 'banana' ]]
            //     ]

            
            test.each(userstable)('should search query for %s', 
                async (email, passwd, phone, fullname, queries) => {

                logger.info('it should search same query for multiple users')

                // for (let u in CREDS) {
                    await GoogleSearch.close()
                    // var email = CREDS[u].email
                    // var passwd = CREDS[u].passwd
                    // var fullname = CREDS[u].fullname

                    logger.debug(`Logging in ${email}`)
                    await GoogleSearch._login(email, passwd)
                    let greeting = `(Bem-vindo|Olá), ${fullname}`
                    let regex = new RegExp(greeting)
                    await expect(GoogleSearch.page).toMatch(regex);
                    
                    await GoogleSearch._open_google_search();
                    await expect(GoogleSearch.page).toMatch(email);

                    logger.debug(`${email} searching for "musica"...`)
                    await GoogleSearch._fill_in_search_string('musica');

                    await GoogleSearch._get_results().then((results) => {
                        const qtd = results.length
                        logger.debug(`${email} search for "musica" returned ${qtd} results`)
                        expect(qtd).toBeGreaterThan(5) 
                    })
                // }

                // done()
            })

        });

        describe('multiple queries multiple users', () => {

            afterAll(() => {
                GoogleSearch.logger.debug('[test] afterAll multiple queries multiple users')
            })

            test.each(userstable)('should search set of different queries for %s', 
                async (email, passwd, phone, fullname, queries) => {

                // for (let u in CREDS) {
                    GoogleSearch.logger.debug('[test] 1. closing page for ' + email)
                    await GoogleSearch.close()

                    // var email = CREDS[u].email
                    // var passwd = CREDS[u].passwd
                    // var fullname = CREDS[u].fullname
                    // var queries = CREDS[u].queries

                    try {
                        GoogleSearch.logger.debug(`[test] Logging in ${email}`)
                        await GoogleSearch._login(email, passwd)
                        GoogleSearch.logger.debug(`[test] ${email} logged in`)
                    } catch (e) {
                        console.log('--------------------- EEEEEERRRRRRRROOOOOOO ---------------------')
                        GoogleSearch.logger.error(`[test] ${email} not authenticated.`)
                        GoogleSearch.logger.error(`[test] ${e}`)
                        throw e
                    }

                    GoogleSearch.logger.debug('[test] User authenticated succesfully')

                    try {
                        let greeting = `(Bem-vindo|Olá), ${fullname}`
                        let regex = new RegExp(greeting)
                        await expect(GoogleSearch.page).toMatch(regex);
                    } catch (e) {
                        GoogleSearch.logger.error(`[test] Asssertion failed: ${e}`)
                        throw e
                    }
                    
                    GoogleSearch.logger.debug('[test] Let\'s search for ' + queries)
                    for (let i in queries) {
                        var q = queries[i]

                        GoogleSearch.logger.debug(`[test] ${email} querying for ${q}`)

                        try {
                            GoogleSearch.logger.debug('[test] Opening google search page')
                            await GoogleSearch._open_google_search();
                            GoogleSearch.logger.debug('[test] Google search page opened')
                            await expect(GoogleSearch.page).toMatch(email);
                        } catch (e) {
                            GoogleSearch.logger.error(`[test] Google search page could not be opened for ${email}.`)
                            GoogleSearch.logger.error(`[test] ${e}`)
                            throw e
                        }

                        try {
                            GoogleSearch.logger.debug(`[test] ${email} searching for "${q}"...`)
                            await GoogleSearch._fill_in_search_string(q);
                            GoogleSearch.logger.debug(`[test] Search for "${q}" done`)
                        } catch (e) {
                            GoogleSearch.logger.error(`[test] Search string could not be filled for ${email}.`)
                            GoogleSearch.logger.error(`[test] ${e}`)
                            throw e
                        }

                        try {
                            GoogleSearch.logger.debug('[test] Getting results...')
                            await GoogleSearch._get_results().then((results) => {
                                const qtd = results.length
                                GoogleSearch.logger.debug(`[test] ${email} search for "${q}" returned ${qtd} results`)
                                expect(qtd).toBeGreaterThan(5) 
                            })
                        } catch (e) {
                            GoogleSearch.logger.error(`[test] Could not get results from "${q}" for ${email}.`)
                            GoogleSearch.logger.error(`[test] ${e}`)
                            throw e
                        }

                        GoogleSearch.logger.debug('[test] Next query...')
                    } // for q
                // } // for u
            }); // test
        });

        
    });
    // test('should...', async (done) => {
    //     logger.debug('it ')
    //     done();
    // });
    
}); // Google authentication

screenshotted = async (eventType, filename) => {
    logger.debug(`event type is: ${eventType}`);
    if (filename) {
        logger.debug(`filename provided: ${filename}`);
    } else {
        logger.debug('filename not provided');
    }
}
