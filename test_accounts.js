const GoogleSearch = require('./main');
const CREDENTIALS = require('./input/credentials');
const winston = require('winston')

const myFormat = winston.format.printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${level}] ${message}`;
});

GoogleSearch._setup('test_accounts')

const tzoffset = (new Date()).getTimezoneOffset() * 60000;
const timestamp = (new Date(Date.now() - tzoffset)).toISOString().substring(0,19 ).replace(/[T\-\:]/g,'');

const logfile_all = `output/test_accounts.${timestamp}.all.log`
const logfile_info = `output/test_accounts.${timestamp}.info.log`

const logger = winston.createLogger({
    level: 'silly',
    format: winston.format.combine(
    winston.format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }),
    winston.format.errors({ stack: true }),
    winston.format.splat(),
    winston.format.json(),
    // winston.format.colorize(),
    myFormat
    ),
    defaultMeta: { service: 'google-search-scraper-accounts-test' },
    transports: [
    // https://github.com/winstonjs/winston#logging-levels
    new winston.transports.File({ filename: logfile_all, level: 'silly' }),
    new winston.transports.File({ filename: logfile_info, level: 'info' }),
    // new winston.transports.Console()
    ]
});


letstest = async () => {
    for (let k in CREDENTIALS) {
        
        let email = CREDENTIALS[k].email
        let passwd = CREDENTIALS[k].passwd
        let phone = CREDENTIALS[k].phone

        logger.info(`Testing ${email} ...............`)
        try {
            await GoogleSearch._login(email, passwd, phone)
            logger.info(`Testing ${email} ............... OK`)
        } catch (e) {
            logger.error(`Testing ${email} ............... NOK`)
            logger.error(`\t${e}`)
            if (e.stack) {
                logger.error(e.stack)
            }
        }
        await GoogleSearch.close()
    }
}

letstest().then((d) => { 
    console.log('-------------------------------')    
    console.log('FIM') 
    console.log('Verifique o resultado em:')
    console.log(`  ${logfile_info}  e`)
    console.log(`  ${logfile_all}\n\n`)
}).catch((r) => { console.log('ERRO: ' + r)})

// Promise.all([letstest()]).then((d) => { console.log('FIM') } )

